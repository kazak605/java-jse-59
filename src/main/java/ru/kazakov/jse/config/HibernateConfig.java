package ru.kazakov.jse.config;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import ru.kazakov.jse.model.*;

public class HibernateConfig {

    private static SessionFactory sessionFactory;

    public HibernateConfig() {
    }

    public static SessionFactory getSessionFactory() {
        if (sessionFactory != null) return sessionFactory;

        Configuration cfg = new Configuration();
        cfg.addAnnotatedClass(Bus.class);
        cfg.addAnnotatedClass(ElectricCar.class);
        cfg.addAnnotatedClass(SportsCar.class);
        cfg.addAnnotatedClass(Truck.class);
        cfg.addAnnotatedClass(Vehicle.class);

        ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
                .applySettings(cfg.getProperties())
                .build();

        sessionFactory = cfg.buildSessionFactory(serviceRegistry);

        return sessionFactory;
    }
}
