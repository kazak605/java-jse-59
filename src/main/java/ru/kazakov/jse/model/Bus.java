package ru.kazakov.jse.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@Entity
@SuperBuilder
@NoArgsConstructor
@Table(name = "bus")
@EqualsAndHashCode(callSuper = true)
public class Bus extends Vehicle {

    @Column(name = "max_passengers", length = 10)
    private Integer maxPassengers;

}
