package ru.kazakov.jse.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@Entity
@SuperBuilder
@NoArgsConstructor
@Table(name = "sports_car")
@EqualsAndHashCode(callSuper = true)
public class SportsCar extends Vehicle {

    @Column(length = 10)
    private Integer horsePower;

    @Column(length = 10)
    private Integer acceleration;

}
