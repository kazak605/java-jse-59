package ru.kazakov.jse;


import ru.kazakov.jse.model.Bus;
import ru.kazakov.jse.model.ElectricCar;
import ru.kazakov.jse.model.SportsCar;
import ru.kazakov.jse.model.Truck;
import ru.kazakov.jse.repository.CommonRepository;
import ru.kazakov.jse.repository.impl.CommonRepositoryImpl;

import java.time.Instant;

public class Application {

    public static void main(String[] args) {
        CommonRepository commonRepository = new CommonRepositoryImpl();
        Bus bus = Bus.builder()
                .brand("AvtoVaz")
                .brand("Ikarus")
                .year(Instant.now())
                .maxPassengers(45)
                .build();
        ElectricCar electricCar = ElectricCar.builder()
                .brand("Tesla")
                .model("Tack")
                .year(Instant.now())
                .maxDistance(599)
                .build();
        SportsCar sportsCar = SportsCar.builder()
                .brand("BMV")
                .model("RX6")
                .year(Instant.now())
                .horsePower(500)
                .acceleration(5)
                .build();
        Truck truck = Truck.builder()
                .brand("BMV")
                .model("Truck1")
                .year(Instant.now())
                .carrying(45)
                .build();
        commonRepository.save(bus);
        commonRepository.save(electricCar);
        commonRepository.save(sportsCar);
        commonRepository.save(truck);
    }

}
